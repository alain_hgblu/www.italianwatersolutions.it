$(function() {
	
	anime({
	  targets: 'img',
	  transform: 'skew(220deg, -150deg)',
	  direction: 'reverse',
	  duration: '1000'
	});

fragsAnim();

});

	

if( 'ontouchstart' in window ){ var click = 'touchstart'; }
	else { var click = 'click'; }


	$('div.burger').on(click, function(){
			if( !$(this).hasClass('open') ){ openMenu(); } 
			else { closeMenu(); }
	});	
	

	$('div.menu ul li a').on(click, function(e){
		e.preventDefault();
		closeMenu();
	});


	function openMenu(){
		
		$('div.menu-bg').addClass('animate');					

		$('div.burger').addClass('open');	
		$('div.x, div.z').addClass('collapse');
		$('.menu li').addClass('animate');
		
		setTimeout(function(){ 
			$('div.y').hide(); 
			$('div.x').addClass('rotate30'); 
			$('div.z').addClass('rotate150'); 
		}, 70);
		setTimeout(function(){
			$('div.x').addClass('rotate45'); 
			$('div.z').addClass('rotate135');  
		}, 120);		
	}
	
	function closeMenu(){

		$('.menu li').removeClass('animate');
		setTimeout(function(){ 			
			$('div.burger').removeClass('open');	
			$('div.x').removeClass('rotate45').addClass('rotate30'); 
			$('div.z').removeClass('rotate135').addClass('rotate150');				
			$('div.menu-bg').removeClass('animate');			
			
			setTimeout(function(){ 			
				$('div.x').removeClass('rotate30'); 
				$('div.z').removeClass('rotate150'); 			
			}, 50);
			setTimeout(function(){
				$('div.y').show(); 
				$('div.x, div.z').removeClass('collapse');
			}, 70);
		}, 100);													
		
	}

















function fragsAnim() {

	$('#Livello_5').children('polygon').each(function(index, el) {
		var pMatrix = $(this).attr('points');
		var ppm = pMatrix.split(' ');
		var ppmN = '';

		$.each(ppm, function(index, val) {
			val = val.trim();
			if (isNaN(val)) {
				
				var vEx = val.split(',');
				var vX = vEx[0],
						vY = vEx[1];


				var rand1 = getRandomInt(10, 25);
				var rand2 = getRandomInt(10, 25);

				var vNew = Number(vX)+rand1 +','+Number(vY)+rand2;
				ppmN = ppmN+' '+vNew;




				/*console.log('X:' + vX);
				console.log('Y:' + vY);*/

			}

		});
		console.log('old: '+pMatrix);
			console.log('new: '+ppmN);

		//$(this).attr('points', ppmN);	
		anime({
		  targets: el,
		  points: ppmN,
		  duration: '3000',
		  direction: 'alternate',
		  easing: 'easeOutQuad',
		  loop: true
		});

		console.log('---');
	});

}









function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}