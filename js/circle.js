var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _ref = _,
    random = _ref.random;

var Vector = function () {
  function Vector(x, y) {
    _classCallCheck(this, Vector);

    this.x = x;
    this.y = y;
  }

  _createClass(Vector, [{
    key: 'add',
    value: function add(v) {
      this.x += v.x;
      this.y += v.y;
      return this;
    }
  }, {
    key: 'mul',
    value: function mul(x) {
      this.x *= x;
      this.y *= x;
      return this;
    }
  }, {
    key: 'dist',
    value: function dist(v) {
      var dx = void 0,
          dy = void 0;
      return Math.sqrt((dx = this.x - v.x) * dx, (dy = this.y - v.y) * dy);
    }
  }, {
    key: 'norm',
    value: function norm() {
      var mag = this.mag;
      return new Vector(this.x / mag, this.y / mag);
    }
  }, {
    key: 'mag',
    get: function get() {
      return Math.sqrt(this.x * this.x, this.y * this.y);
    },
    set: function set(v) {
      var n = this.norm();
      this.x = n.x * v;
      this.y = n.y * v;
    }
  }], [{
    key: 'fromPolar',
    value: function fromPolar(r, t) {
      return new Vector(r * Math.cos(t), r * Math.sin(t));
    }
  }]);

  return Vector;
}();

var Noise = function () {
  function Noise(w, h, oct) {
    _classCallCheck(this, Noise);

    this.width = w;
    this.height = h;
    this.octaves = oct;
    this.canvas = Noise.compositeNoise(w, h, oct);
    var ctx = this.canvas.getContext('2d');
    this.data = ctx.getImageData(0, 0, w, h).data;
  }

  // create w by h noise


  _createClass(Noise, [{
    key: 'getNoise',


    // returns noise from -1.0 to 1.0
    value: function getNoise(x, y, ch) {
      // bitwise ~~ to floor
      var i = (~~x + ~~y * this.width) * 4;
      return this.data[i + ch] / 127 - 1;
    }
  }], [{
    key: 'noise',
    value: function noise(w, h) {
      var cv = document.createElement('canvas'),
          ctx = cv.getContext('2d');

      cv.width = w;
      cv.height = h;

      var img = ctx.getImageData(0, 0, w, h),
          data = img.data;

      for (var i = 0, l = data.length; i < l; i += 4) {
        data[i + 0] = random(0, 255);
        data[i + 1] = random(0, 255);
        data[i + 2] = random(0, 255);
        data[i + 3] = 255;
      }

      ctx.putImageData(img, 0, 0);
      return cv;
    }

    // create composite noise with multiple octaves

  }, {
    key: 'compositeNoise',
    value: function compositeNoise(w, h, oct) {
      var cv = document.createElement('canvas'),
          ctx = cv.getContext('2d');

      cv.width = w;
      cv.height = h;

      ctx.fillStyle = '#000';
      ctx.fillRect(0, 0, w, h);

      ctx.globalCompositeOperation = 'lighter';
      ctx.globalAlpha = 1 / oct;

      for (var i = 0; i < oct; i++) {
        var _noise = Noise.noise(w >> i, h >> i);
        ctx.drawImage(_noise, 0, 0, w, h);
      }

      return cv;
    }
  }]);

  return Noise;
}();

var Particle = function () {
  function Particle(x, y) {
    var vx = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
    var vy = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0;

    _classCallCheck(this, Particle);

    this.pos = new Vector(x, y);
    this.vel = new Vector(vx, vy);
    this.acc = new Vector(0, 0);
    // this.tick = 0
    // this.life = random(100, 300)
  }

  _createClass(Particle, [{
    key: 'update',
    value: function update(noise) {
      // this.tick++
      // if (this.tick > this.life)
      //   return

      this.pos.add(this.vel);

      var _pos = this.pos,
          x = _pos.x,
          y = _pos.y;

      var dx = noise.getNoise(x, y, 0),
          dy = noise.getNoise(x, y, 1);

      // this.vel.add(this.acc)
      this.vel.add(new Vector(dx, dy));
      // this.acc.add(new Vector(dx / 10, dy / 10))
      // this.acc.mul(0.95)
      this.vel.mul(0.95);
    }
  }, {
    key: 'draw',
    value: function draw(ctx) {
      // if (this.tick > this.life) return
      ctx.fillRect(this.pos.x, this.pos.y, 2, 2);
    }
  }]);

  return Particle;
}();

function init() {
  noise = new Noise(w, h, 8);
  // document.body.appendChild(noise.canvas)
  particles = [];

  for (var i = 0; i < 10000; i++) {
    var r1 = w / 4,
        //random(w / 4 - 100, w / 4, true),
    a1 = random(0, 2 * Math.PI, true),
        r2 = random(0, 1, true),
        a2 = random(0, 2 * Math.PI, true);

    var pos = Vector.fromPolar(r1, a1),
        vel = Vector.fromPolar(r2, a2);

    pos.add(new Vector(w / 2, h / 2));
    //     let x = random(0, w, true),
    //         y = random(0, h, true),
    //         vx = random(-1, 1, true),
    //         vy = random(-1, 1, true)

    //     let pos = new Vector(x, y),
    //         vel = new Vector(vx, vy)

    particles.push(new Particle(pos.x, pos.y, vel.x, vel.y));
  }

  ctx.fillStyle = '#ecf0f1';
  ctx.fillRect(0, 0, w, h);

  ctx.fillStyle = 'rgba(0, 121, 148, 0.05)';

  animate();
}

// click once to pause, twice to regen
function generate() {
  if (rid) {
    window.cancelAnimationFrame(rid);
    rid = 0;
  } else {
    init();
  }
}

function render() {
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = particles[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var p = _step.value;

      p.update(noise);
      p.draw(ctx);
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }
}

function animate() {
  // for (let i = 0; i < 10; i++)
  render();

  rid = window.requestAnimationFrame(animate);
}

var w = innerWidth * devicePixelRatio,
    h = innerHeight * devicePixelRatio,
    noise = void 0,
    particles = void 0,
    rid = void 0,
    cv = document.createElement('canvas'),
    ctx = cv.getContext('2d');

cv.width = w;
cv.height = h;

document.body.appendChild(cv);
cv.addEventListener('mousedown', generate);
cv.addEventListener('touchstart', generate);

init();