<section>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>
					Siamo un pool di aziende<br>con <span>l’acqua nel dna.</span>
				</h2>
				<h3>
					Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua.
				</h3>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<img class="img-responsive" src="imgs/infographic01.png" alt="">
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur.
				</p>
			</div>
		</div>
	</div>
</section>

<section id="partners-desc">
	<div class="container">
		<div class="row">
			<div class="col-md-6 p-desc">
				<img src="imgs/partners/2F_logo.png" alt="">
				<h5>IDROSTUDI</h5>
				<p>
					Nel settore delle reti di acquedotto e fognatura fornisce servizi altamente specializzati e finalizzati all’ottimizzazione della gestione. La società, negli anni, ha ingegnerizzato il know how universitario nazionale ed internazionale relativo alle reti tecnologiche.
				</p>
				<a href="http://www.2fwaterventure.it">www.2fwaterventure.it</a>
			</div>

			<div class="col-md-6 p-desc">
				<img src="imgs/partners/marchio-BM.png" alt="">
				<h5>BM TECNOLOGIE INDUSTRIALI</h5>
				<p>
					Nelle varie fasi del processo del ciclo idrico integrato si occupa principalmente della fornitura di strumentazione per la misura di portata, livello, pressione e misura dei parametri chimico-fisici dell’acqua. Svolge inoltre campagne di misure della portata per la ricerca di acque parassite e la realizzazione di distretti idrici degli acquedotti.
				</p>
				<a href="http://www.bmtecnologie.it">www.bmtecnologie.it</a>
			</div>

			<div class="col-md-6 p-desc">
				<img src="imgs/partners/marchio-etc-1.png" alt="">
				<h5>ETC SUSTAINABLE SOLUTIONS</h5>
				<p>
					Offre le migliori soluzioni tecnologiche a gestori ed installatori di impianti di trattamento mirate al risparmio energetico, al recupero delle risorse e alla sostenibilità degli interventi in senso lato. La gamma di prodotti OSCAR, dedicata al trattamento acque reflue, rappresenta oggi il punto di riferimento del mercato dei controllori di processo.
				</p>
				<a href="http://www.etc-eng.it">www.etc-eng.it</a>
			</div>

			<div class="col-md-6 p-desc">
				<img src="imgs/partners/MARCHIO-IDEA-1.png" alt="">
				<h5>IDEA</h5>
				<p>
					In tutte le fasi del ciclo idrico integrato si occupa di sistemi di telecontrollo ed automazione fornendo quadri con RTU (con alimentazione e low power) o PLC di tutti i maggiori produttori, sistemi di trasmissione, software SCADA eventualmente integrati ai programmi necessari nel processo.
				</p>
				<a href="http://www.idea-srl.it">www.idea-srl.it</a>
			</div>

			<div class="col-md-6 p-desc">
				<img src="imgs/partners/marchio-idrostudi.png" alt="">
				<h5>IDROSTUDI</h5>
				<p>
					Nel settore delle reti di acquedotto e fognatura fornisce servizi altamente specializzati e finalizzati all’ottimizzazione della gestione. La società, negli anni, ha ingegnerizzato il know how universitario nazionale ed internazionale relativo alle reti tecnologiche.
				</p>
				<a href="http://www.2fwaterventure.it">www.2fwaterventure.it</a>
			</div>

			<div class="col-md-6 p-desc">
				<img src="imgs/partners/tae_logo.jpg" alt="">
				<h5>IDROSTUDI</h5>
				<p>
					Nel settore delle reti di acquedotto e fognatura fornisce servizi altamente specializzati e finalizzati all’ottimizzazione della gestione. La società, negli anni, ha ingegnerizzato il know how universitario nazionale ed internazionale relativo alle reti tecnologiche.
				</p>
				<a href="http://www.2fwaterventure.it">www.2fwaterventure.it</a>
			</div>



		</div>
	</div>
</section>