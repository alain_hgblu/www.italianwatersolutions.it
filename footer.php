		<footer class="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<img id="logo" class="img-responsive" src="imgs/iws_logo_w.svg" alt="">
					</div>
				</div>
			</div>
		</footer>

		<script src="node_modules/jquery/dist/jquery.min.js"></script>
		<script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="node_modules/animejs/anime.min.js"></script>
		<script src="js/script.js"></script>

	</body>
</html>