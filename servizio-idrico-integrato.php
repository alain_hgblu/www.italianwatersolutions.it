<section>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>
					<span>L’acqua è il nostro bene più prezioso.</span><br>
					Per questo ce ne prendiamo cura ogni giorno.
				</h2>
				<h3>
					Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua.
				</h3>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, est asperiores ut vitae culpa molestiae rem nemo, sint, natus eum assumenda iste inventore incidunt ex quas illum doloremque. Quidem, aspernatur. 
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, est asperiores ut vitae culpa molestiae rem nemo, sint, natus eum assumenda iste inventore incidunt ex quas illum doloremque. Quidem, aspernatur.
				</p>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, est asperiores ut vitae culpa molestiae rem nemo, sint, natus eum assumenda iste inventore incidunt ex quas illum doloremque. Quidem, aspernatur.
				</p>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<img src="imgs/brands.png" alt="" class="img-responsive">
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<img src="imgs/infographic02.jpg" alt="" class="img-responsive">
			</div>
		</div>
	</div>
</section>

<!-- 
	<div class="col cb-teal">&nbsp;</div>
	<div class="col cb-blue">&nbsp;</div>
	<div class="col cb-yellow">&nbsp;</div>
	<div class="col cb-green">&nbsp;</div>
	<div class="col cb-red">&nbsp;</div>
	<div class="col cb-liblue">&nbsp;</div>
-->

<section id="servizi">
	<div class="container">
		<div class="row">

			<div class="col-md-6">
				<div class="card">
					<div class="row card-border">
						<div class="col cb-teal">&nbsp;</div>
						<div class="col cb-blue">&nbsp;</div>
						<div class="col cb-yellow">&nbsp;</div>
						<div class="col ">&nbsp;</div>
						<div class="col ">&nbsp;</div>
						<div class="col ">&nbsp;</div>
					</div>
					<div class="card-content">
						<h6>Captazione</h6>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. Quis aute iure reprehenderit.
						</p>
					</div>
				</div>

				<div class="card">
					<div class="row card-border">
						<div class="col cb-blue">&nbsp;</div>
						<div class="col cb-yellow">&nbsp;</div>
						<div class="col cb-green">&nbsp;</div>
						<div class="col ">&nbsp;</div>
						<div class="col ">&nbsp;</div>
						<div class="col ">&nbsp;</div>
					</div>
					<div class="card-content">
						<h6>Captazione</h6>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. Quis aute iure reprehenderit.
						</p>
					</div>
				</div>

				<div class="card">
					<div class="row card-border">
						<div class="col cb-blue">&nbsp;</div>
						<div class="col cb-yellow">&nbsp;</div>
						<div class="col cb-red">&nbsp;</div>
						<div class="col ">&nbsp;</div>
						<div class="col ">&nbsp;</div>
						<div class="col ">&nbsp;</div>
					</div>
					<div class="card-content">
						<h6>Captazione</h6>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. Quis aute iure reprehenderit.
						</p>
					</div>
				</div>

				<div class="card">
					<div class="row card-border">
						<div class="col cb-blue">&nbsp;</div>
						<div class="col cb-yellow">&nbsp;</div>
						<div class="col cb-green">&nbsp;</div>
						<div class="col cb-teal">&nbsp;</div>
						<div class="col ">&nbsp;</div>
						<div class="col ">&nbsp;</div>
					</div>
					<div class="card-content">
						<h6>Captazione</h6>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. Quis aute iure reprehenderit.
						</p>
					</div>
				</div>

				<div class="card">
					<div class="row card-border">
						<div class="col cb-blue">&nbsp;</div>
						<div class="col cb-yellow">&nbsp;</div>
						<div class="col cb-green">&nbsp;</div>
						<div class="col ">&nbsp;</div>
						<div class="col ">&nbsp;</div>
						<div class="col ">&nbsp;</div>
					</div>
					<div class="card-content">
						<h6>Captazione</h6>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. Quis aute iure reprehenderit.
						</p>
					</div>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="card">
					<div class="row card-border">
						<div class="col cb-blue">&nbsp;</div>
						<div class="col cb-yellow">&nbsp;</div>
						<div class="col cb-green">&nbsp;</div>
						<div class="col ">&nbsp;</div>
						<div class="col ">&nbsp;</div>
						<div class="col ">&nbsp;</div> 
					</div>
					<div class="card-content">
						<h6>Captazione</h6>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. Quis aute iure reprehenderit.
						</p>
					</div>
				</div>

				<div class="card">
					<div class="row card-border">
						<div class="col cb-teal">&nbsp;</div>
						<div class="col cb-blue">&nbsp;</div>
						<div class="col cb-yellow">&nbsp;</div>
						<div class="col cb-red">&nbsp;</div>
						<div class="col ">&nbsp;</div>
						<div class="col ">&nbsp;</div>
					</div>
					<div class="card-content">
						<h6>Captazione</h6>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. Quis aute iure reprehenderit.
							Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. Quis aute iure reprehenderit.
						</p>
					</div>
				</div>

				<div class="card">
					<div class="row card-border">
						<div class="col cb-teal">&nbsp;</div>
						<div class="col cb-blue">&nbsp;</div>
						<div class="col cb-yellow">&nbsp;</div>
						<div class="col cb-red">&nbsp;</div>
						<div class="col ">&nbsp;</div>
						<div class="col ">&nbsp;</div>
					</div>
					<div class="card-content">
						<h6>Captazione</h6>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. Quis aute iure reprehenderit.
						</p>
					</div>
				</div>

				<div class="card">
					<div class="row card-border">
						<div class="col cb-blue">&nbsp;</div>
						<div class="col cb-yellow">&nbsp;</div>
						<div class="col cb-green">&nbsp;</div>
						<div class="col ">&nbsp;</div>
						<div class="col ">&nbsp;</div>
						<div class="col ">&nbsp;</div>
					</div>
					<div class="card-content">
						<h6>Captazione</h6>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. Quis aute iure reprehenderit.
						</p>
					</div>
				</div>

			</div>



		</div>
	</div>
</section>