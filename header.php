<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<link href="https://fonts.googleapis.com/css?family=Cairo:200,400,700" rel="stylesheet">
	
	<link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/custom.css">
</head>
<body>
	<?php 
		$navClass = ($intNav) ? 'navbar-internal' : '' ;
		$na = ['','','',''];

		switch ($nActive) {
			case 'chi-siamo':
				$na[1] = 'active';
				break;

			case 'servizio-idrico-integrato':
				$na[2] = 'active';
				break;
			
			default:
				$na[0] = 'active';
				break;
		}
 	?>
	<nav class="navbar navbar-expand-lg navbar-light <?php echo $navClass ?>">
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	
		<div class="container">
			<a class="navbar-brand" href="?p"><img  src="imgs/iws_logo.svg" alt=""></a>
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav ml-auto">
		      <li class="nav-item <?php echo $na[0] ?>">
		        <a class="nav-link" href="?p">Home <span class="active-bar"></span></a>
		      </li>
		      <li class="nav-item <?php echo $na[1] ?>">
		        <a class="nav-link" href="?p=chi-siamo">Chi siamo <span class="active-bar"></span></a>
		      </li>
		      <li class="nav-item <?php echo $na[2] ?>">
		        <a class="nav-link" href="?p=servizio-idrico-integrato">Servizio idrico integrato <span class="active-bar"></span></a>
		      </li>
		      <li class="nav-item <?php echo $na[3] ?>">
		        <a class="nav-link" href="#">Contatti <span class="active-bar"></span></a>
		      </li>
		    </ul>
		  </div>
		</div>
	</nav>




<div class="navbar"></div>
<div class="list">
  <div class="item">
    <div class="img"></div>
    <span></span>
    <span></span>
    <span></span>
  </div>
  <div class="item">
    <div class="img"></div>
    <span></span>
    <span></span>
    <span></span>
  </div>
  <div class="item">
    <div class="img"></div>
    <span></span>
    <span></span>
    <span></span>
  </div>
  <div class="item">
    <div class="img"></div>
    <span></span>
    <span></span>
    <span></span>
  </div>
</div>
<div class="menu-bg"></div>
<div class="menu">
  <ul class="menu-splitL">
    <li><a href="">About</a></li>
    <li><a href="">Share</a></li>
    <li><a href="">Activity</a></li>
    <li><a href="">Settings</a></li>
    <li><a href="">Contact</a></li>
  </ul>
  <ul class="menu-splitR">
    <li><a href="">About</a></li>
    <li><a href="">Share</a></li>
    <li><a href="">Activity</a></li>
    <li><a href="">Settings</a></li>
    <li><a href="">Contact</a></li>
  </ul>
</div>
<div class="burger">
  <div class="x"></div>
  <div class="y"></div>
  <div class="z"></div>
</div>