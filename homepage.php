
<section id="masterhead">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h2>Soluzioni di successo<br> per i Clienti del <br><span>Ciclo Idrico Integrato</span></h2>
				<p>
					Frase evocativa sull’idea di gruppo integrato.<br>
					IWS è in grado di proporsi come interlocutore unico nei confronti dei gestori del C.I.I. per la fornitura di prodotti e servizi.
				</p>
			</div>
			<div class="col-md-6" id="drop">
				<?php echo file_get_contents('imgs/drop.svg'); ?>
			</div>
		</div>
	</div>
</section>

<section class="grey">
	<div class="container">
		<div class="row row-eq-height">
			<div class="col-md-6">
				<img class="img-responsive" src="imgs/sink.png" alt="">
			</div>
			<div class="col-md-6 bg-light p-4 news">
				<div class="date">02 FEBBRAIO 2018</div>
				<h4>PARTECIPAZIONE FIERA / NEWS</h4>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				</p>
				<button class="btn">LEGGI TUTTO</button>
			</div>
		</div>
	</div>
</section>


<section>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus fugiat iusto sit culpa, voluptas optio tempora. Itaque commodi, harum sequi iste maiores, nostrum temporibus dolores veniam libero autem laboriosam officia? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus fugiat iusto sit culpa, voluptas optio tempora. Itaque commodi, harum sequi iste maiores, nostrum temporibus dolores veniam libero autem laboriosam officia?
				</p>
			</div>
		</div>
	</div>
</section>

<section id="partners">
	<div class="container">
		<div class="row">
			<div class="col-md-2 text-center">
				<img class="img-responsive" src="imgs/partners/Logo_ENV.png" alt="">
			</div>
			<div class="col-md-2 text-center">
				<img class="img-responsive" src="imgs/partners/marchio-BM.png" alt="">
			</div>
			<div class="col-md-2 text-center">
				<img class="img-responsive" src="imgs/partners/marchio-etc-1.png" alt="">
			</div>
			<div class="col-md-2 text-center">
				<img class="img-responsive" src="imgs/partners/MARCHIO-IDEA-1.png" alt="">
			</div>
			<div class="col-md-2 text-center">
				<img class="img-responsive" src="imgs/partners/marchio-idrostudi.png" alt="">
			</div>
			<div class="col-md-2 text-center">
				<img class="img-responsive" src="imgs/partners/tae_logo.jpg" alt="">
			</div>
		</div>
	</div>
</section>